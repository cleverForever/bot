from django.db import models


class Message(models.Model):
    message_id = models.IntegerField(
        verbose_name='ID сообщения',
        null=False, blank=False
    )
    chat_id = models.IntegerField(
        verbose_name='ID чата',
        null=False, blank=False
    )
    type = models.TextField(
        verbose_name='Тип',
        null=False, blank=False
    )
