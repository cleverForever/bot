from django.apps import AppConfig


class BotModConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bot_mod'
