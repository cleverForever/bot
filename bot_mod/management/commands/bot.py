from random import randint

import telebot
from django.core.management.base import BaseCommand

from bot.settings import bot
from bot_mod.models import Message

answers = ['забирай', 'держи', 'получай', 'на', 'получите, распишитесь', 'вам письмо']
types = ['photo', 'document', 'text']


class Command(BaseCommand):
    help = 'Телеграмм бот '

    @bot.message_handler(commands=['start'])
    def start_message(message):
        bot.send_message(message.chat.id,
                         text="Короче, дабы мой создатель наконец научился работать с докером "
                              "и деплоить приложения на всякие хостинги, я буду помогать тебе сортировать всю фигню, "
                              "которую ты мне пришлёшь\n"
                              "Переодически я могу отваливаться, но это связано со слабыми возможностями сервера "
                              "и оченеь странной ошибкой телеги, которую мой создатель так и не понял, как решить")
        qs = Message.objects.filter(chat_id=message.chat.id)
        if qs.exists():
            qs.delete()

    @bot.message_handler(commands=['type'])
    def type_message(message):
        markup = telebot.types.InlineKeyboardMarkup()
        qs = Message.objects.filter(chat_id=message.chat.id)
        if qs.filter(type=types[0]):
            markup.add(telebot.types.InlineKeyboardButton(text='картинка', callback_data='photo'))
        if qs.filter(type=types[1]):
            markup.add(telebot.types.InlineKeyboardButton(text='документы', callback_data='document'))
        if qs.filter(type=types[2]):
            markup.add(telebot.types.InlineKeyboardButton(text='текст', callback_data='text'))
        if qs.filter(type__in=types):
            markup.add(telebot.types.InlineKeyboardButton(text='удалить всё', callback_data='delete'))
            bot.send_message(message.chat.id, text="Что нужно?", reply_markup=markup)
        else:
            bot.send_message(message.chat.id, text="У тебя нет ничего, впрочем, тут ты на моего создателя похож.\n"
                                                   "Отправь мне документ, фото или текст")

    @bot.message_handler(content_types=['photo', 'document', 'text'])
    def command_handle_document(message):
        bot.send_message(message.chat.id,
                         'АйДишка твоего сообщения записана. Не переживай, я записываю только АйДишки')
        type = message.content_type
        obj = dict(message_id=message.message_id, chat_id=message.chat.id, type=type)
        Message.objects.create(**obj)

    @bot.callback_query_handler(func=lambda call: True)
    def query_handler(call):
        qs = Message.objects.filter(chat_id=call.message.chat.id)
        if call.data == 'delete':
            if qs.exists():
                bot.answer_callback_query(callback_query_id=call.id,
                                          text='Все АйДишки были удалены')
                qs.delete()
            else:
                bot.answer_callback_query(callback_query_id=call.id,
                                          text='Товарищь, верь, я могу что-то удалить, если ты мне что-то пришлёшь. ')
        else:
            bot.answer_callback_query(callback_query_id=call.id, text='Ща выложу, погодь')
            messages = qs.filter(type=call.data)
            for m in messages:
                ans = randint(0, len(answers) - 1)
                bot.send_message(call.message.chat.id, reply_to_message_id=m.message_id, text=answers[ans])

    def handle(self, *args, **options):
        bot.polling(none_stop=True, interval=0)
